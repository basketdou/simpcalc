# simpcalc

Simpcalc is a compact and simple calculator. It also includes simpconverter, an easy converter of measure units.
### Simpcalc features
- solves ax^2+bx+c=0
- exponentiation
- does arithmetic operations
### Simpconverter features
- converts Celsius temperature to Fahrenheit and Kelvin
- converts Fahrenheit temperature to Kelvin and Celsius
- converts kilometres to miles and vice versa
- converts km to m and vice versa
- converts cm to metres and vice versa
- converts kilos to grams and vice versa
- converts tons to kilos and vice versa
- converts a decimal number to a binary one and vice versa
### To do (simpcalc)
- trigonometric functions
- trigonometric equations
- plotting functions
### To do (simpconverter)
- a decimal number to a hexadecimal one conversion and vice versa
- volume units conversion
- digital information units conversion
- currency conversion
- interactive mode
## Downloads
The latest version of it can be found in "Tags". Download attached zip and unpack it. Run 'chmod +x simpcalc' in terminal. Now you can run calc by entering a command './simpcalc' in terminal.
## How to build
Download this branch (the unstable one) and unpack it. 
Then run in terminal 
``` sh
$ cd simpcalc-unstable
$ make all
$ ./simpcalc -h
or
$ ./simpconverter -h
```
