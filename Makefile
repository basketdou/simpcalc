CC=g++
CFLAGS=-Wall -Wextra

all: 
	mkdir bin
	$(CC) src/simpcalc/main.cpp src/simpcalc/TimeCalc.cpp src/simpcalc/logtrigon.cpp -o bin/simpcalc
	$(CC) src/simpconverter/converter.cpp src/simpconverter/numconv.cpp -o bin/simpconverter
	ln -s bin/simpcalc bin/calc
	ln -s bin/simpconverter bin/converter
debug:
	mkdir bin
	$(CC) $(CFLAGS) src/simpcalc/main.cpp src/simpcalc/TimeCalc.cpp src/simpcalc/logtrigon.cpp -o bin/simpcalc
	$(CC) $(CFLAGS) src/simpconverter/converter.cpp src/simpconverter/numconv.cpp -o bin/simpconverter
	ln -s bin/simpcalc bin/calc
	ln -s bin/simpconverter bin/converter
calc:
	mkdir bin
	$(CC) src/simpcalc/main.cpp src/simpcalc/TimeCalc.cpp src/simpcalc/logtrigon.cpp -o bin/simpcalc
	ln -s bin/simpcalc bin/calc
converter:
	mkdir bin
	$(CC) src/simpconverter/converter.cpp src/simpconverter/numconv.cpp -o bin/simpconverter
	ln -s bin/simpconverter bin/converter
clean:
	rm -rf bin
