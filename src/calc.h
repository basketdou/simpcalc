// simpcalc
int add(double a, double b);
int subtr(double a, double b);
int multip(double a, double b);
int divide(double a, double b);
int exponent(double a);
int simplog(double a);
int calc();

int TimeCalc();
uint32_t log(uint32_t a, uint32_t b);
int lognum();
int loge();
int ctg(double angle);
int simptrigon();
int arcfunc();
int deg2rad();
int rad2deg();
// simpconverter
int dectobin();
int bintodec();

float cubmetrestolitres(float amount);
float litrestocubmetres(float amount);
float barreltolitres(float amount); 
float ukgallontousgallon(float amount);
float usgallontoukgallon(float amount);
float oilbarreltousgallon(float amount);
float usgallontooilbarrel(float amount);

float celsiustokel(float amount); 
float celsiustofahr(float amount); 
float fahrenheittokelvin(float amount);
float fahrtocelsius(float amount);
