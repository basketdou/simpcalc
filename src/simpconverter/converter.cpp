#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <cstring>
#include "../calc.h"
#define CONVERTER_VERSION "0.2.0"

float amount;
// The functions that are used in mass unit conversion
float fromkilomeasures(float amount) {
    return amount*1000;
}
float tokilomeasures(float amount) {
    return amount*0.001; 
}
// Distance units
float metrestocm(float amount) {
    return amount*100;
}
float cmtometres(float amount) {
    return amount*0.01;
}
// Distance units
float kmtomiles(float amount) {
    float kmtom = amount/1.60934;
    return kmtom;
}
float miletokm(float amount) {
    float mtokm = amount*1.60934;
    return mtokm;
}
// Volume units
float cubmetrestolitres(float amount) {
    float cubmetrtol = amount*1000;
    return cubmetrtol;
}
float litrestocubmetres(float amount) {
    float ltocubmetr = amount/1000;
    return ltocubmetr;
}
float barreltolitres(float amount) {
    float barreltol = amount*158.987294928;
    return barreltol;
}
float ukgallontousgallon(float amount) {
    float ukgal2usgal = amount*1.201;
    return ukgal2usgal;
}
float usgallontoukgallon(float amount) {
    float usgal2ukgal = amount/1.201;
    return usgal2ukgal;
}
float oilbarreltousgallon(float amount) {
    float oilbarreltousgal = amount*42;
    return oilbarreltousgal;
}
float usgallontooilbarrel(float amount) {
    float usgaltooilbarrel = amount/42;
    return usgaltooilbarrel;
}
// Temperature units
float celsiustokel(float amount) {
    float ctok = 273.15;
    return amount+ctok;
}
float celsiustofahr(float amount) {
    float x = (amount*9/5)+32;
    return x;
}
float fahrenheittokelvin(float amount) {
    float ftok = (amount - 32)*5/9+273.15;
    return ftok;
}
float fahrtocelsius(float amount) {
    float ftoc = (amount - 32) * 5/9;
    return ftoc;
}
//int draft() {
//    char measure[4], target[8];
//    float amount;
//    std::cout << "Enter a measure to convert from :\n";
//    std::cin >> measure;
//    std::cout << "Enter a measure to convert to" << std::endl;
//    std::cin >> target;
//    if ((measure == "meter") || (measure == "meters") || (measure == "m")) {
//        if ((target == "km") || (target == "kilometers") || (target == "kilometer")) {
//            std::cout << amount << " m = " << fromkilomeasures(amount) << " km\n";
//   }
//}
//return 0;
//}
int getamount() {
    std::cout << "Enter an amount to convert from" << std::endl;
    std::cin >> amount;
    return 0;
}
void help() {
    std::cerr << "Usage: simpconverter [OPTIONS] \n"
    << "OPTIONS\n"
    << "-h               Show this message\n"
    << "-v               Show version\n"
    << "Units of temperature\n"
    << "-f -k  (-fk)     Convert fahrenheit to kelvin\n"
    << "-f -c  (-fc)     Convert fahrenheit to celsius\n"
    << "-c -f  (-cf)     Convert celsius to fahrenheit\n"
    << "-c -k  (-ck)     Convert celsius to kelvin\n"
    << "Units of mass\n"
    << "-g -kg (-gk)     Convert grams to kilos\n"
    << "-kg -g (-kgg)    Convert kilos to grams\n"
    << "-t -kg (-tk)     Convert tons to kilos\n"
    << "-kg -t (-kt)     Convert kilos to tons\n"
    << "Units of distance\n"
    << "-km -mil         Convert kilometres to miles\n"
    << "-mil -km         Convert miles to kilometres\n"
    << "-km -m           Convert kilometres to metres\n"
    << "-m -km           Convert metres to kilometres\n"
    << "-m -cm (-mc)     Convert metres to centimetres\n"
    << "-cm -m (-c2m)    Convert centimetres to metres\n"
    << "Units of volume\n"
    << "-m3 -l (-m3l)    Convert cubic metres to litres\n"
    << "-l -m3 (-lm3)    Convert litres to cubic litres\n"
    << "-b -l            Convert barrel to litres\n"
    << "-ukg -usg        Convert UK gallon to US gallon\n"
    << "-usg -ukg        Convert US gallon to UK gallon\n"
    << "-ob -usg         Convert oil barrel to US gallon\n"
    << "-usg -ob         Convert US gallon to oil barrel\n"
    << "Numeral systems\n"
    << "-dec -bin (-db)      Convert a decimal number to binary\n"
    << "-bin -dec (-bd)      Convert a binary number to decimal\n"
    << std::endl;
}
int main(int argc, char *argv[]) {
    std::vector<std::string> args(argv, argv+argc);
    for (size_t u = 1;u < args.size();u++) {
        if ((args[u] == "--help") || (args[u] == "-h")) {
            help();
        }
        if (args[1] == "-v") {
            std::cout << "simpconverter, a part of simpcalc, version " << CONVERTER_VERSION << std::endl;
        }
        if (((args[1] == "-g") && (args[2] == "-kg")) || (args[1] == "-gk")) {
            getamount();
            std::cout << amount << " g = " << tokilomeasures(amount) << " kg" << std::endl;
        }
        if (((args[1] == "-c") && (args[2] == "-f")) || (args[1] == "-cf")) {
            getamount();
            std::cout << amount << " C = " << celsiustofahr(amount) << " F\n";
        }
        if (((args[1] == "-t") && (args[2] == "-kg")) || (args[1] == "-tk")) {
            getamount();
            std::cout << amount << " t = " << fromkilomeasures(amount) << " kg" << std::endl;
        }
        if (((args[1] == "-kg") && (args[2] == "-t")) || (args[1] == "-kt")) {
            getamount();
            std::cout << amount << " kg = " << tokilomeasures(amount) << " t" << std::endl;
            // ru-ton
        }
        if (((args[1] == "-c") && (args[2] == "-k")) || (args[1] == "-ck")) {
            getamount();
            std::cout << amount << " C = " << celsiustokel(amount) << " K\n";
        }
        if (((args[1] == "-f") && (args[2] == "-k")) || (args[1] == "-fk")) {
            getamount();
            std::cout << amount << " F = " << fahrenheittokelvin(amount) << " K\n";
        }
        if (((args[1] == "-f") && (args[2] == "-c")) || (args[1] == "-fc")) {
            getamount();
            std::cout << amount << " F = " << fahrtocelsius(amount) << " C\n";
        }
        if (((args[1] == "-kg") && (args[2] == "-g")) || (args[1] == "-kgg")) {
            getamount();
            std::cout << amount << " kg = " << fromkilomeasures(amount) << " g\n";
        }
        if ((args[1] == "-km") && (args[2] == "-mil")) {
            getamount();
            std::cout << amount << " km = " << kmtomiles(amount) << " miles\n";
        }
        if ((args[1] == "-mil") && (args[2] == "-km")) {
            getamount();
            std::cout << amount << " miles = " << miletokm(amount) << " km\n";
        }
        if ((args[1] == "-km") && (args[2] == "-m")) {
            getamount();
            std::cout << amount << " km = " << fromkilomeasures(amount) << " m\n";
        }
        if ((args[1] == "-m") && (args[2] == "-km")) {
            getamount();
            std::cout << amount << " m = " << tokilomeasures(amount) << " km\n";
        }
        if (((args[1] == "-m") && (args[2] == "-cm")) || (args[1] == "-mc")) {
            getamount();
            std::cout << amount << " m = " << metrestocm(amount) << " cm\n";
        }
        if (((args[1] == "-cm") && (args[2] == "-m")) || (args[1] == "-c2m")) {
            getamount();
            std::cout << amount << " cm = " << cmtometres(amount) << " m\n";
        }
        if ((args[1] == "-dec") && (args[2] == "-bin")) {
            dectobin();
        }
        if ((args[1] == "-bin") && (args[2] == "-dec")) {
            bintodec();
        }
        // Volume units
        if (((args[1] == "-m3") && (args[2] == "-l")) || (args[1] == "-m3l")) {
            getamount();
            std::cout << amount << " m3 = " << cubmetrestolitres(amount) << " l\n";
        }
        if (((args[1] == "-l") && (args[2] == "-m3")) || (args[1] == "-lm3")) {
            getamount();
            std::cout << amount << " l = " << litrestocubmetres(amount) << " m3\n";
        }
        if ((args[1] == "-b") && (args[2] == "-l")) {
            getamount();
            std::cout << amount << " barrel = " << barreltolitres(amount) << " l\n";
        }
        if ((args[1] == "-ukg") && (args[2] == "-usg")) {
            getamount();
            std::cout << amount << " UK gal = " << ukgallontousgallon(amount) << " US gal\n";
        }
        if ((args[1] == "-usg") && (args[2] == "-ukg")) {
            getamount();
            std::cout << amount << " US gal = " << usgallontoukgallon(amount) << " UK gal\n";
        }
        if ((args[1] == "-ob") && (args[2] == "-usg")) {
            getamount();
            std::cout << amount << " oil barrel = " << oilbarreltousgallon(amount) << " US gal\n";
        }
        if ((args[1] == "-usg") && (args[2] == "-ob")) {
            getamount();
            std::cout << amount << " US gal = " << usgallontooilbarrel(amount) << " oil barrel\n";
        }
    }
    return 0;
}
