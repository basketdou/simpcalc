#include <cmath>
#include <iostream>
#include <string>
#include "../calc.h"
const double pi = 3.14159265359;

int ctg(double angle){
    return 1/(tan(angle));
}
int simptrigon() {
    double angle;
    char userchoice[2];
    std::cout << "Enter an angle in degrees: " << std::endl;
    std::cin >> angle;
    std::cout << "Choose what to calculate:" << std::endl 
    << "(S)ine, (C)osine, (T)angent, c(O)tangment" << "\n";
    std::cin >> userchoice;
    if ((userchoice == "S") || (userchoice == "s")) {
        std::cout << "sin(" << angle << ") = " << sin(angle) << std::endl;
    }
    if ((userchoice == "C") || (userchoice == "c")) {
        std::cout << "cos(" << angle << ") = " << cos(angle) << std::endl;
    }
    if ((userchoice == "T") || (userchoice == "t")) {
        std::cout << "tan(" << angle << ") = " << tan(angle) << std::endl;
    }
    if ((userchoice == "O") || (userchoice == "o")) {
        std::cout << "ctg(" << angle << ") = " << ctg(angle) << std::endl;
    }
    return 0;
}
int arcfunc() {
    double x;
    std::cout << "Enter a number to calculate arc function of: " << std::endl;
    std::cin >> x;
    std::string choice;
    std::cout << "What to calculate? arcsin? arccos? arctan (arctg)?" << std::endl 
    << "Enter the name of the function to calculate:" << std::endl;
    std::cin >> choice;
    if (choice == "arcsin") {
        std::cout << "arcsin(" << x << ") = " << asin(x) << std::endl;
    }
    if (choice == "arccos") {
        std::cout << "arccos(" << x << ") = " << acos(x) << std::endl;
    }
    if (choice == "arctan" || (choice == "arctg")) {
        std::cout << "arctan(" << x << ") = " << atan(x) << std::endl;
    }
    return 0;
}
int deg2rad() {
    float deg, rad;
    std::cout << "Enter an angle in degrees: \n";
    std::cin >> deg;
    rad = 0.0174532925*deg;
    std::cout << deg << " degrees = " << rad << " radians\n";
    return 0;
}
int rad2deg() {
    float deg,rad;
    std::cout << "Enter radians: \n";
    std::cin >> rad;
    deg = rad/0.0174532925;
    std::cout << rad << " radians = " << deg << " degrees\n";
    return 0;
}
uint32_t log(uint32_t a, uint32_t b) {    
    return log2(a)/log2(b);
}

int lognum() {
    uint32_t a,b;
    std::cout << "Enter the number to get a logarithm of:" << "\n";
    std::cin >> a;
    std::cout << "Enter the base of the number to get a logarithm of:" << "\n";
    std::cin >> b;
    std::cout << "The logarithm of " << a << " base " << b << " is " << log(a,b) << "\n";
    return 0;
}
int loge() {
    uint32_t x;
    std::cout << "Enter the number to get natural logarithm of: " << std::endl;
    std::cin >> x;
    std::cout << "The natural logarithm of " << x << " is " << log(x) << std::endl;
    return 0;
}
