#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include "../calc.h"
#define CALC_VERSION "0.1.8"
using namespace std;
int add(double a, double b) {
    return a+b;
}
int subtr(double a, double b) {
     return a-b;
}
int multip(double a, double b) {
     return a*b;
}
int divide(double a, double b) {
    return a/b;
}
int exponent(double a) {
    double y,d;
    cout << "Exponentation\n";
    cout << "Enter an exponent. if 0<first number<1 = roots.\n";
    cin >> d;
    y = pow(a,d);
    cout << a << "^" << d << " = " << y << "\n";
    return 0;
}

int calc() {
    int choice;
    double a, b;
    std::cout << "Choose an action\n";
    std::cout << "1 for addition, 2 for subtraction, 3 for multiplying, 4 for division\n";
    std::cin >> choice;
    std::cout << "Enter a first number.\n";
    std::cin >> a;
    while(choice != 5){
        std::cout << "Enter a second number.\n";
        std::cin >> b;
        if (choice == 1) {
            std::cout << a << " + " << b << " = " << add(a,b) <<"\n";
        }
        if (choice == 2) {
            std::cout << a << " - " << b << " = " << subtr(a,b) <<"\n";
        }
        if (choice == 3) {
            std::cout << a << " * " << b << " = " << multip(a,b) <<"\n";
        }
        if (choice == 4) {
            std::cout << a << " / " << b << " = " << divide(a,b) <<"\n";
        }
        break;
    }
    return 0;
}
void help() {
    std::cerr << "Usage: simpcalc [OPTION] "<< "\n"
    << "OPTIONS\n"
    << "-a       Calculate arc functions of trigonometry" << std::endl
    << "-d       Convert a decimal to a radian" << "\n" 
    << "-e       Exponentation" << "\n"
    << "-h or --help       Show this message" << "\n"
    << "-l       Calculate a logarithm of a number"<< "\n"
    << "-n       Calculate natural logarithm"<< std::endl
    << "-t       Calculate time\n"
    << "-tr      Calculate trigonometrical functions\n"
    << "-r       Convert a radian to a decimal"<< std::endl
    << "-s       Do simple calc" << std::endl
    << "-t       Calculate time\n"
    << "-v       Show version" << std::endl;
}

int main(int argc, char *argv[]) {
    double a;
    std::vector<std::string> args(argv, argv+argc);
    for (size_t r = 1;r < args.size(); r++) {
        if ((args[r] == "-h") || (args[r] == "--help")|| (argc == 0)) {
            help();
        }
        if (args[1] == "-a") {
            arcfunc();
        }
        if (args[1] == "-d") {
            deg2rad();
        }
        if (args[1] == "-e") {
            std::cout << "Enter a number:";
            std::cin >> a;
            std::cout << "\n";
            exponent(a);
        }
        if (args[1] == "-l") {
            lognum();
        }
        if (args[1] == "-n") {
            loge();
        }
        if (args[1] == "-tr") {
            simptrigon();
        }
        if (args[1] == "-r") {
            rad2deg();
        }
        if (args[1] == "-s") {
            calc();
        }
        if (args[1] == "-t") {
            TimeCalc();
        }
        if (args[1] == "-v") {
            std::cout << "simpcalc version " << CALC_VERSION << std::endl;
        }
    }
}
