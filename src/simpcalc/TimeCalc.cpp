#include <iostream>
#include <cstdio>
#include <ctime>
#include "../calc.h"
int TimeCalc() {
    int hours,minutes,seconds;
    std::time_t curr_time;
    curr_time = time(NULL);
    
    std::tm *tm_local = std::localtime(&curr_time);
    int curr_hour = tm_local->tm_hour;
    int curr_min = tm_local->tm_min;
    int curr_sec = tm_local->tm_sec;
    printf("Current time is %02d : %02d : %02d \n", curr_hour, curr_min, curr_sec);
    std::cout << "Enter amount of hours to add or subtract (negative numbers to subtract):\n";
    std::cin >> hours;
    std::cout << "Enter amount of minutes to add or subtract (negative numbers to subtract):\n";
    std::cin >> minutes;
    std::cout << "Enter amount of seconds to add or subtract (negative numbers to subtract):\n";
    std::cin >> seconds;
    printf("The result is %02d : %02d : %02d \n", curr_hour+hours, curr_min+minutes, curr_sec+seconds);
    return 0;
}
